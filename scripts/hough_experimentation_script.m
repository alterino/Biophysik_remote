
% listing = dir('
close all 
for i = 3:length(listing)-1
    img = imread( fullfile( listing(i).folder, listing(i).name ) );
    
    I = img;
    I = mat2gray(I);
    I = max(I - imgaussfilt(I,350),0);
    I = mat2gray(I);
    bw = I > graythresh(I);
    bw = imclose(bw,strel('disk',3));
    bw = bwareaopen(bw,5);
    
    [H,T,R] = hough(bw);
    figure 
    
    P  = houghpeaks(H,10,'threshold',ceil(0.1*max(H(:))),'Nhoodsize',[105,31]);
    
    
    subplot(1,2,1)
    imshow(H,[],'XData',T,'YData',R,'InitialMagnification','fit');
    xlabel('\theta'), ylabel('\rho');
    axis on, axis normal, hold on;
    plot(T(P(:,2)),R(P(:,1)),'s','color','white');

    subplot(1,2,2)

    lines = houghlines(bw,T,R,P,'FillGap',2,'MinLength',20);
    imagesc(bw), hold on
    max_len = 0;
    for k = 1:length(lines)
        xy = [lines(k).point1; lines(k).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
        hold on
        % Plot beginnings and ends of lines
        plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
        plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
        
        % Determine the endpoints of the longest line segment
        len = norm(lines(k).point1 - lines(k).point2);
        if ( len > max_len)
            max_len = len;
            xy_long = xy;
        end
    end
    %    I1 = mat2gray(img);
    %    I = I1 - imgaussfilt(I1,150);
    %    I = max(0,I);
    %    bw = I> graythresh(I);
    %
    %
    %
    %
    %    bw = imclose(bw,ones(3));
    %    bw = bwareaopen(bw,10);
    %
    %    rp = regionprops(rp,'centroid','majoraxislength','orientation');
    
    
    
    
    
 
    
end