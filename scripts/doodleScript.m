img_dir = dir( '/home/mmarino/pile/OS_Biophysics/Microscopy/20170715/fluor*.tiff' );

for i = 1:length( img_dir )
   
   img = imread( strcat( img_dir(i).folder, '/', img_dir(i).name ) );
   imtool(img, [] );
   
end