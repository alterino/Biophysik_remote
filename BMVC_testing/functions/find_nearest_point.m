function [correspondent_matrix, distances, stats] = ...
    find_nearest_point( true_pts, test_pts )
%find_nearest_point finds the corresponding nearest points on a 1D line
%for vectors consisting of the same amount of values

stats = struct('num_true', [], 'num_missed', [], 'num_false', [],...
                'missed_idx', [], 'false_idx', [] );
dists = zeros(length(test_pts), length(true_pts))+inf;
distance_threshold = 25;

for i = 1:length(true_pts)
    for j = 1:length(test_pts)
        dists(j,i) = abs(true_pts(i)-test_pts(j));
    end
end

truept_idx = zeros(length(true_pts), 1);
testpt_idx = zeros(length(true_pts), 1);
done_idx = [];
% done_idx = zeros(length(true_pts), 1);
col_idxes = 1:length(true_pts);
row_idxes = 1:length(test_pts);

chosen_count = 0;
done_idx = [];
while( chosen_count < length(test_pts) && chosen_count < length(true_pts) )
    % rows correspond to ground truth points, columns to test points
   [row, col] = find(dists == min(min(dists)));
   
   if(min(min(dists)) > distance_threshold)
       stats.missed_idx = col_idxes;
       stats.false_idx = row_idxes;
       break
   end
   
   chosen_count = chosen_count+1;
   testpt_idx(chosen_count) = row_idxes(row);
   truept_idx(chosen_count) = col_idxes(col);
   dists(row, :) = [];
   dists(:, col) = [];

   row_idxes(row) = [];
   col_idxes(col) = [];
   
end

testpt_idx(testpt_idx == 0) = [];
truept_idx(truept_idx == 0) = [];
correspondent_matrix = [testpt_idx, truept_idx];
distances = true_pts(truept_idx) - test_pts(testpt_idx);

stats.num_missed = length(stats.missed_idx);
stats.num_false = length(stats.false_idx);
stats.num_true = length(true_pts);