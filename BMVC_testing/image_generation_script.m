close all, clear all, clc
% generate pattern template images for figure
PC_tag = computer;

if( strcmp( PC_tag, 'GLNXA64' ) )
    output_dir = '/home/mmarino/Documents/MATLAB/OS_Biophysik_repo/BMVC_testing/paper_imgs/';
else
    output_dir = '/home/michael/Documents/MATLAB/OS_Biophysik_repo/BMVC_testing/paper_imgs/';
end

num_across = 4;
num_down = 4;

%% circular island pattern
curr_shape = 'circles';
gridded_pattern = [];

for i=1:num_down
    temp_row = [];
    for j=1:num_across
        A=zeros(102,102);
        r = 20; %radius
        m = {51,51}; %midpoint
        A(m{:})=1;
        B = bwdist(A) <= r;
%         imshow(B)
        temp_row = [temp_row, B];
    end
    gridded_pattern = [gridded_pattern; temp_row];
end


figure(1), imshow(gridded_pattern, []);
saveas(gcf, strcat( output_dir, curr_shape, '_pattern' ), 'epsc');

%% square island pattern

curr_shape = 'squares';
gridded_pattern = [];

for i=1:num_down
    temp_row = [];
    for j=1:num_across
        A=ones(40,40);
        A = padarray(A, [40 40], 0, 'both');
        temp_row = [temp_row, A];
    end
    gridded_pattern = [gridded_pattern; temp_row];
end

imshow(gridded_pattern, []);
saveas(gcf, strcat( output_dir, curr_shape, '_pattern' ), 'epsc');
clear gridded_pattern temp_row A i j

%% stripe pattern

curr_shape = 'stripes';
img_dims = [500, 500];
stripe_width = 25;
space_width = 50;

pattern = floures_pattern_gen_2( stripe_width, space_width, img_dims, 6 );
imshow(pattern, []);
saveas(gcf, strcat( output_dir, curr_shape, '_pattern' ), 'epsc');
clear img_dims stripe_width space_width

%% checkered stripe pattern
curr_shape = 'checkered';

pattern_2 = imrotate(pattern, 90);
pattern_combined = or( pattern, pattern_2 );
imshow(pattern_combined, []);
saveas(gcf, strcat( output_dir, curr_shape, '_pattern' ), 'epsc');

clear pattern* curr_shape

%% converting flourescent tiff files to eps

tiff_dir = dir( strcat( output_dir, '*.tif' ) );

for i = 1:length(tiff_dir)
   img = imread( strcat( tiff_dir(i).folder, '/', tiff_dir(i).name ) ); 
   imshow(img, []);
   saveas(gcf, strcat( output_dir, 'stripe_example', num2str(i) ), 'epsc');
end



































