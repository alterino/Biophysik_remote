close all, clear, clc

if( strcmp(computer, 'GLNXA64') )
    labels_dir = dir( '/home/mmarino/data/stripe_labels/*.mat');
else
    labels_dir = dir( '/media/michael/Data/OS_Biophysik/Microscopy/BMVC_testing/*.mat');
end
    
test_type = 'radon';

results = cell(length(labels_dir), 1);
label_generators = cell(length(labels_dir), 1);

for i = 1:length( labels_dir )
   
    temp_dir = strcat( labels_dir(i).folder, '/', labels_dir(i).name );
    
    % passing a zero makes GUI invisible
    labelGenerator = LabelGenerator(0);
    load_image(labelGenerator, temp_dir);
    
    results{i} = test_algorithm(labelGenerator, test_type);
    label_generators{i} = labelGenerator;
    
    
    
    
    
end

tot_stripes = 0;
tot_missed = 0;
tot_false = 0;
mean_distance = 0;

for i = 1:length(results)
    temp_results = results{i};
    
    tot_stripes = tot_stripes + temp_results.overview.total_stripes;
    tot_missed = tot_missed + temp_results.overview.missed;
    tot_false = tot_false + temp_results.overview.false;
    mean_distance = mean_distance +...
       temp_results.overview.mean_distance*temp_results.overview.total_stripes;
end

missed_rate = tot_missed/tot_stripes;
mean_distance = mean_distance/tot_stripes;