classdef TestManager < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % test parameters
        intensity_thresh
        size_thresh = 2000;
        manual_theta = [];
        manual_width = [];
        algorithm_type
        
        % results
        stats = struct('num_true', [],...
                       'num_missed', [],... 
                       'num_false', [],...
                       'missed_idx', [],...
                       'false_idx', []);
        overview = struct('total_stripes', [],...
                          'detected', [],...
                          'missed', [],...
                          'false', [],...
                          'mean_distance', [] ); 
        theta_estimate
        width_estimate
        centers
        stack_centers
        test_distances
        correspondencies
        
    end
    
    methods
        
        function results = radon_test(this, img_stack, true_centers_stack)
            this.width_estimate = zeros( size(img_stack, 3), 1 );
            this.theta_estimate = zeros( size(img_stack, 3), 1 )-inf;
            this.stack_centers = cell( size(img_stack, 3), 1 );
            
            % estimate optimal threshold for theta/width estimation
            img_vec = img_stack(:);
            img_vec(img_vec == 0) = [];
            this.intensity_thresh = multithresh( img_vec, 1 );
            img_dims = [size(img_stack, 1), size(img_stack, 2)];
            std_pattern = floures_pattern_gen( floor(this.manual_width), 0, img_dims, 1);
            if( this.manual_theta < 0 ), rot_angle = this.manual_theta + 90;
            else, rot_angle = this.manual_theta - 90; end
            
            std_pattern = imrotate(std_pattern, rot_angle);
            for j = 1:size(img_stack, 3)
                img = img_stack(:,:,j);
                img_var = var( var( double( img ) ) );
                
                if( img_var < 1e5 )
                    continue
                end
                
%                 bw_fluor = threshold_fluor_img( img,...
%                                                 this.intensity_thresh,...
%                                                 this.size_thresh );
%                 [thetaD, pattern, x_guess, width_guess] =...
%                                    est_pattern_orientation( img, bw_fluor );
%                 
%                 pattern = imrotate(pattern, rot_ang);
                thetaD = [];
                if( ~isempty( thetaD ) )
                    this.width_estimate(j) = width_guess;
                    this.theta_estimate(j) = thetaD;
%                     bw_dic = ones( size( img ), 'logical' );
                    [x, x_p, y, x_dists] = find_stripe_locations( thetaD, img, pattern );
                    
                    this.stack_centers{j} = x_p;
                else
                    [x, x_p, y, x_dists] = find_stripe_locations( this.manual_theta,...
                                                                  img,...
                                                                  std_pattern );
                    this.stack_centers{j} = x_p;
                end
            end
            
            calculate_results(this, true_centers_stack);
            results = this;
        end
        
        function calculate_results(this, true_centers_stack)
           this.test_distances = [];
           this.correspondencies = cell(length(true_centers_stack), 1);
%            this.correspondences = cell(length(true_centers_stack), 1);
           for i = 1:length(true_centers_stack)
               true_centers = true_centers_stack{i};
               test_centers = this.stack_centers{i};
               
                [corresponds, dists, statsy] =...
                     find_nearest_point(true_centers, test_centers);

                this.correspondencies{i} = corresponds;
                this.test_distances = [this.test_distances; dists];
                this.stats(i) = statsy;
           end
            
           evaluate_results(this);
        end
        
        function evaluate_results(this)
            
            this.overview.total_stripes = 0;
            this.overview.missed = 0;
            this.overview.false = 0;
            
            for i = 1:length(this.stats)
               this.overview.total_stripes =...
                   this.overview.total_stripes + this.stats(i).num_true;
               this.overview.missed =...
                   this.overview.missed + this.stats(i).num_missed;
               this.overview.false = ...
                   this.overview.false + this.stats(i).num_false;
            end
            
            this.overview.mean_distance = mean( abs(this.test_distances) );
        end
        
    end
end

