classdef LabelGenerator < handle
    properties
        screen_lims
        h_fig
        h_panel_main
        h_panel_images
        h_axes
        
        h_panel_buttons
        h_panel_controls
        h_panel_analysis
        h_panel_filter
        h_panel_label
        h_controls
        h_center_slider
        h_theta_slider
        h_width_slider
        image_panel_dims = [1,1];
        stretch_bool = 0; % keep images square or stretch them
        default_label_count = 0;
        
        % suppress GUI for scripting usage
        GUI_bool
        % suppress negative images for suppression
        suppress_bool
        
        theta_text;
        width_text;
        center_text;
        idx_text;
        stripe_idx_text;
        stats_text;
        
        img_stack = [];
        image_labels
        image_directory;
        image_filename;
        stack_idx = 0;
        
        theta = -90;
        width = 50;
        centers = [];
        test_centers;
        stack_centers;
        stripe_locations;
        stripe_set_bool;
        curr_stripe_idx;
        pts_plot;
        test_pts_plot;
        export_struct = struct('stack', [], 'theta', [], 'width', [], 'centers', [] );
        test_bool = 0;
        testManager = [];
        results;
        
    end
    % UI interface
    methods
        %% initialization functions
        % constructor
        function this = LabelGenerator(GUI_bool)
            if(~exist('GUI_bool', 'var'))
                GUI_bool = 1;
            end
            this.screen_lims = get(0, 'ScreenSize');
            fig_pos = [150, 200,...
                this.screen_lims(3)-400, this.screen_lims(4)-200];
            
            this.h_fig = figure('Units', 'pixels',...
                'Name', 'Analysis Toolbox',...
                'OuterPosition', fig_pos);
            % for running in script
            if(~GUI_bool)
                this.h_fig.Visible = 'off';
            end
            this.GUI_bool = GUI_bool;
            this.h_panel_main = uipanel(this.h_fig,...
                'Units','normalized',...
                'Position',[0 0 3/4 1],...
                'backgroundcolor','w');
            this.h_panel_buttons = uipanel(this.h_fig,...
                'Units','normalized',...
                'Position',[3/4 19/20 1/4 1/20],...
                'backgroundcolor','w');
            this.h_panel_controls = uipanel(this.h_fig,...
                'Units','normalized',...
                'Position',[3/4 0.5 1/4 0.45],...
                'backgroundcolor','w');
            this.h_panel_analysis = uipanel(this.h_fig,...
                'Units','normalized',...
                'Position',[3/4 0 1/4 0.5],...
                'backgroundcolor','w');
            
            this.image_directory = pwd;
            
            arrange_image_panel(this);
            setup_controls(this);
        end
        
        function arrange_image_panel(this)
            rows = this.image_panel_dims(1);
            cols = this.image_panel_dims(2);
            
            %             this.h_panel_images = cell(this.image_panel_dims);
            %             this.h_axes= cell(this.image_panel_dims);
            
            %             for i = 1:this.image_panel_dims(2)
            %                 for j = 1:this.image_panel_dims(1)
            i = 1;
            j = 1;
            this.h_panel_images = uipanel( this.h_panel_main,...
                'Units', 'normalized',...
                'Position', [(i-1)/cols,(rows-j)/rows,1/cols,1/rows],...
                'backgroundcolor', 'w');
            if(this.stretch_bool)
                this.h_axes = axes( 'Parent', this.h_panel_images,...
                    'Units', 'normalized',...
                    'Position', [0 0 1 1],...
                    'XTick', [], 'YTick', [],...
                    'Box', 'on');
            end
            
            if(~this.stretch_bool)
                panel_pos = getpixelposition(this.h_panel_images);
                % making axes dimensions square
                panel_dims = [panel_pos(3), panel_pos(4)];
                %                     axes_dims = [min(panel_dims), min(panel_dims)];
                mindim_idx = find( panel_dims == min(panel_dims) );
                switch mindim_idx
                    case 1
                        axes_pos = [1, 1+(panel_dims(2)-panel_dims(1))/2,...
                            min(panel_dims), min(panel_dims)];
                    case 2
                        axes_pos = [1+(panel_dims(1)-panel_dims(2))/2, 1,...
                            min(panel_dims), min(panel_dims)];
                    otherwise
                        error('dimension index should not be greater than 2')
                end
                
                this.h_axes = axes( 'Parent', this.h_panel_images,...
                    'Units', 'pixels',...
                    'Position', axes_pos,...
                    'XTick', [], 'YTick', [],...
                    'Box', 'on');
            end
            
        end
        function setup_controls(this)
            
            uicontrol(...
                'Parent',this.h_panel_buttons,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [0/3 0 1/3 1],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Load Image',...
                'Callback', @(src,evnt)load_image(this));
            uicontrol(...
                'Parent',this.h_panel_buttons,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [1/3 0 1/3 1],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Export',...
                'Callback', @(src,evnt)export_labels(this));
            uicontrol(...
                'Parent',this.h_panel_buttons,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [2/3 0 1/3 1],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Test',...
                'Callback', @(src,evnt)test_algorithm(this));
            
            setup_label_controls(this);
        end
        function setup_label_controls(this)
            if( isempty( this.h_panel_label ) )
                this.h_panel_label = uipanel(this.h_panel_controls,...
                    'Units','normalized', 'Position', [0 0 1 1],...
                    'backgroundcolor','w',...
                    'Visible', 'on');
            else
                set( this.h_panel_label, 'Visible', 'on' );
            end
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [2/4 6/10 1/4 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Add',...
                'Callback', @(src,evnt)add_stripe(this));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [0/4 6/10 1/4 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Remove',...
                'Callback', @(src,evnt)remove_stripe(this));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [1/4 6/10 1/4 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Set',...
                'Callback', @(src,evnt)set_stripe(this));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [3/4 6/10 1/4 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.3,...
                'String', 'Load Test',...
                'Callback', @(src,evnt)load_testdata(this));
            
            this.theta_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 3/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String',sprintf('theta = %d', this.theta) );
            this.idx_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 8/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String',sprintf('image idx: %i/%i', 0, 0) );
            this.width_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 4/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String',sprintf('width = %d', this.width));
            this.center_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 5/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String','center control' );
            this.stripe_idx_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 1/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String',sprintf('stripe idx: %i', 0) );
            this.stats_text = uicontrol(this.h_panel_label,...
                'Style','text',...
                'Units', 'normalized', 'Position',[0 1/10 1/2 1/10],...
                'FontUnits', 'normalized',...
                'FontSize', 0.4,...
                'String',sprintf('stripe idx: %i', 0) );
            
            this.h_theta_slider = uicontrol(this.h_panel_label,...
                'Style', 'slider',...
                'Style', 'slider',...
                'Min',-90,...
                'Max',90,...
                'Value', this.theta,...
                'SliderStep',[0.001 0.1],...
                'Units', 'normalized',...
                'Position', [1/2, 3/10, 1/2, 1/10],...
                'Callback', @(src, evnt) update_theta(this, src, evnt) );
            
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [3/4, 8/10, 1/4, 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.6,...
                'String', '>>',...
                'Callback', @(src,evnt) shift_stack_idx(this, 1));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [2/4, 8/10, 1/4, 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.6,...
                'String', '<<',...
                'Callback', @(src,evnt) shift_stack_idx(this, -1));
            
            this.h_width_slider = uicontrol(this.h_panel_label,...
                'Style', 'slider',...
                'Style', 'slider',...
                'Min',1,...
                'Max',100,...
                'Value', this.width,...
                'SliderStep',[.002 .05],...
                'Units', 'normalized',...
                'Position', [1/2, 4/10, 1/2, 1/10],...
                'Callback', @(src, evnt) update_width(this, src, evnt) );
            
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [3/4, 1/10, 1/4, 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.6,...
                'String', '>',...
                'Callback', @(src,evnt) shift_stripe_idx(this, 1));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [2/4, 1/10, 1/4, 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.6,...
                'String', '<',...
                'Callback', @(src,evnt) shift_stripe_idx(this, -1));
            
            this.h_center_slider = uicontrol(this.h_panel_label,...
                'Style', 'slider',...
                'Style', 'slider',...
                'Min', 0,...
                'Max',1,...
                'Value', 0.5,...
                'SliderStep',[.01 .1],...
                'Units', 'normalized',...
                'Position', [1/2, 5/10, 1/2, 1/10],...
                'Callback', @(src, evnt) update_center(this, src, evnt) );
            
            uicontrol(this.h_panel_label,...
                'Style','checkbox',...
                'String', 'Suppress Negatives', ...
                'Value', 0,...
                'Units','normalized',...
                'Position', [0/2, 0/10, 1/2, 1/10],...
                'Callback',@(src, evnt) suppress_negatives(this, src));
            uicontrol(...
                'Parent',this.h_panel_label,...
                'Style', 'pushbutton',...
                'Units','normalized',...
                'Position', [1/2, 0/10, 1/2, 1/10],...
                'FontUnits','normalized',...
                'FontSize', 0.4,...
                'String', 'Export Figure',...
                'Callback', @(src,evnt) export_figure(this));
            
        end
        
        %% callback functions
        function load_image(this, img_dir)
            % temporary folder setting - just set to home directory for
            % ease of testing. sorry if this caused an error because Im a
            % douche.
            if(~exist('img_dir', 'var'))
                
                if( strcmp( computer, 'GLNXA64' ) )
                    img_dir = '/home/mmarino/data/stripe_labels';
                else
                    img_dir = '/media/michael/Data/OS_Biophysik/Microscopy';
                end
                
                [filename, pathname, filter_idx] = uigetfile(...
                    {'*.tif'; '*.tiff'; '*.mat'},...
                    'Select TIFF image file', img_dir);
                this.image_directory = pathname;
                this.image_filename = filename;
                img_dir = strcat(pathname, '/', filename);
            else
                filter_idx = 3;
            end
            %             temp_img_dir = '/home/mmarino/pile/OS_Biophysics/Microscopy/170706';
            
            if( filter_idx > 3 )
                if( ~(filter_idx==3 && strcat(img_dir(end-3:end), '.mat')) )
                    error('expected TIFF image or .mat file')
                end
            end
            
            this.testManager = [];
            this.test_bool = 0;
            if( filter_idx < 3 )
                img_dim = input('enter image dimension: ');
                img = imread( img_dir );
                this.img_stack = img_2D_to_img_stack( img, [img_dim, img_dim] );
                this.stack_centers = cell(size( this.img_stack, 3), 1 );
                this.stack_idx = 1;
                this.width = 50;
                this.theta = -90;
                this.centers = [];
            elseif( filter_idx == 3 )
                data = load( img_dir );
                parse_struct(this, data.label_struct);
                img_dim = size(this.img_stack, 1);
                this.centers = this.stack_centers{this.stack_idx};
                this.stripe_set_bool = ones(length(this.centers), 1);
                this.h_width_slider.Value = this.width;
                this.width_text.String = sprintf('width = %.1f', this.width);
                this.h_theta_slider.Value = this.theta;
                this.theta_text.String = sprintf('theta = %.1f', this.theta);
            end
            this.idx_text.String = sprintf('image idx: %i/%i',...
                this.stack_idx,...
                size(this.img_stack,3));
            
            update_axes(this);
            %             update_stripe_extents(this);
            
            this.h_center_slider.Min = -20;
            this.h_center_slider.Max = img_dim+20;
            this.h_center_slider.SliderStep = [1 30]/(img_dim+40);
            
        end
        
        function load_testdata(this)
            if( strcmp( computer, 'GLNXA64' ) )
                img_dir = '/home/mmarino/data/stripe_labels';
            else
                img_dir = '/media/michael/Data/OS_Biophysik/Microscopy';
            end
            
            [filename, pathname, filter_idx] = uigetfile(...
                {'*.mat'},...
                'Select .mat file', img_dir);
            if( filter_idx > 1 )
                error('expected .mat file');
            end
            
            load( strcat( pathname, '/', filename ), 'results', 'labels_dir' );
            data_idx = 0;
            for i = 1:length(labels_dir)
                if( strcmp( this.image_filename(1:end-4), labels_dir(i).name(1:end-4) ) )
                    data_idx = i;
                end
            end
            
            if( data_idx==0 )
                error('no matching data found in file')
            end
            
            this.testManager = results{data_idx};
            this.test_bool = 1;
            update_axes(this);
        end
        
        function set_stripe(this)
            if( this.curr_stripe_idx > 0 )
                for i = 1:length(this.pts_plot)
                    this.pts_plot(i).XData = [];
                    this.pts_plot(i).YData = [];
                end
                this.stripe_set_bool(this.curr_stripe_idx) = 1;
                this.curr_stripe_idx = 0;
                this.stack_centers{this.stack_idx} = this.centers;
                this.curr_stripe_idx = 0;
                this.stripe_idx_text.String = sprintf('stripe idx: %i', this.curr_stripe_idx);
                update_stripe_extents(this);
            else
                warning('currently no stripe being edited');
            end
        end
        function add_stripe(this)
            for i = 1:length(this.pts_plot)
                this.pts_plot(i).XData = [];
                this.pts_plot(i).YData = [];
            end
            this.pts_plot = [];
            if( isempty(this.centers) )
                this.centers = [this.centers;  size(this.img_stack, 2)/4];
            else
                this.centers = [this.centers;  min(this.centers(end)+135,...
                    size(this.img_stack, 2))];
            end
            this.stack_centers{this.stack_idx} = this.centers;
            this.stripe_set_bool = ones( length(this.centers), 1);
            this.curr_stripe_idx = length(this.centers);
            this.h_center_slider.Value = this.centers(this.curr_stripe_idx);
            this.stripe_set_bool(this.curr_stripe_idx) = 0;
            this.stripe_idx_text.String = sprintf('stripe idx: %i', this.curr_stripe_idx);
            update_stripe_extents(this);
        end
        function remove_stripe(this)
            for i = 1:length(this.pts_plot)
                this.pts_plot(i).XData = [];
                this.pts_plot(i).YData = [];
            end
            this.pts_plot = [];
            this.centers(this.curr_stripe_idx) = [];
            this.stripe_set_bool = ones( length(this.centers), 1);
            if(isempty(this.centers))
                this.curr_stripe_idx = 0;
            else
                this.curr_stripe_idx = 1;
                this.stripe_set_bool(this.curr_stripe_idx) = 0;
            end
            this.stripe_idx_text.String = sprintf('stripe idx: %i', this.curr_stripe_idx);
            this.stack_centers{this.stack_idx} = this.centers;
            update_stripe_extents(this);
        end
        function results = test_algorithm(this, test_type)
            %             this.image_panel_dims = [1,2];
            %             this.arrange_image_panel();
            this.testManager = TestManager;
            %             img_stack = this.img_stack;
            this.testManager.manual_theta = this.theta;
            this.testManager.manual_width = this.width;
            % placeholder for generalization
            if(~exist('test_type', 'var'))
                test_type = 'radon';
            end
            
            if strcmp(test_type, 'radon')
                fprintf('testing Radon implementation... ')
                results = radon_test(this.testManager, this.img_stack, this.stack_centers);
                fprintf('...test complete.\n')
                this.test_bool = 1;
            else
                results = [];
            end
            update_axes(this);
            
        end
        function suppress_negatives(this, src)
            this.suppress_bool = src.Value;
        end
        function export_figure(this)
            directory =...
                '/home/mmarino/Documents/MATLAB/OS_Biophysik_repo/BMVC_testing/paper_imgs/';
            filename = input('enter filename for .eps file: ', 's');
%             axes(this.h_axes);
            fig_temp = figure('Visible','on');
            newAxes = copyobj(this.h_axes,fig_temp);
            resize_pos = get(fig_temp, 'Position');
            set(fig_temp, 'Position', resize_pos);
%             set(newAxes,'Position',resize_pos); % The original position is copied too, so adjust it.
%             set(fig_temp,'CreateFcn','set(gcbf,''Visible'',''on'')'); % Make it visible upon loading
            
            saveas(newAxes, strcat( directory, filename ), 'epsc');
            delete(fig_temp);
        end
        
        
        
        %% update functions
        function update_axes(this)
            cla(this.h_axes);
            this.pts_plot = [];
            
            set(this.h_axes, 'Units', 'pixels');
            resize_pos = get(this.h_axes, 'Position');
            img_resized = imresize( this.img_stack(:,:,this.stack_idx),...
                [resize_pos(4), resize_pos(3)] );
            
            imshow( img_resized, [], 'Parent', this.h_axes );
            update_stripe_extents(this);
        end
        function update_stripe_extents(this)
            if((isempty(this.centers) && isempty(this.test_centers)) || ~this.GUI_bool)
                return
            end
            dims = size(this.img_stack, 1);
            rotation_mat = [cosd(this.theta), -sind(this.theta);...
                sind(this.theta), cosd(this.theta)];
            if(~isempty(this.centers))
                % create parallel vertical lines at stripe edges
                locations_concat = cell(length(this.centers), 2);
                pts_set = [];
                pts_working = [];
                for i = 1:length( this.centers )
                    temp_x = (1:dims*2)-dims;
                    temp_y = zeros(1, dims*2);
                    
                    % generate stripe locations for left and right of center
                    rotated_pts_1 = rotation_mat*[temp_x; temp_y];
                    rotated_pts_1(1,:) = rotated_pts_1(1,:) + this.centers(i)...
                        - this.width/2*sind(this.theta);
                    rotated_pts_1(2,:) = rotated_pts_1(2,:) + dims/2 ...
                        + this.width/2*cosd(this.theta);
                    [~, out_of_bounds] = find( rotated_pts_1 < 1 );
                    rotated_pts_1(:, out_of_bounds) = [];
                    [~, out_of_bounds] = find( rotated_pts_1 > dims );
                    rotated_pts_1(:, out_of_bounds) = [];
                    
                    rotated_pts_2 = rotation_mat*[temp_x; temp_y];
                    rotated_pts_2(1,:) = rotated_pts_2(1,:) + this.centers(i)...
                        + this.width/2*sind(this.theta);
                    rotated_pts_2(2,:) = rotated_pts_2(2,:) + dims/2 ...
                        - this.width/2*cosd(this.theta);
                    [~, out_of_bounds] = find( rotated_pts_2 < 1 );
                    rotated_pts_2(:, out_of_bounds) = [];
                    [~, out_of_bounds] = find( rotated_pts_2 > dims );
                    rotated_pts_2(:, out_of_bounds) = [];
                    
                    if( this.stripe_set_bool(i) == 1  )
                        pts_set = [pts_set, rotated_pts_1, rotated_pts_2];
                    elseif( isempty( pts_working) )
                        pts_working = [rotated_pts_1, rotated_pts_2];
                    else
                        error('multiple stripes currently not set')
                    end
                    
                    locations_concat{i, 1} = rotated_pts_1;
                    locations_concat{i, 2} = rotated_pts_2;
                end
                if(isempty(pts_working) && length(this.pts_plot)==2 )
                    this.pts_plot(2) = [];
                end
                this.stripe_locations = locations_concat;
                resize_pos = get(this.h_axes, 'Position');
                dims = size(this.img_stack, 1);
                if(isempty(this.pts_plot) && ~isempty(pts_set) && ~isempty(pts_working) )
                    axes(this.h_axes), hold on
                    this.pts_plot = plot( pts_set(1,:)*resize_pos(3)/dims,...
                        pts_set(2,:)*resize_pos(4)/dims, 'b.',...
                        pts_working(1,:)*resize_pos(3)/dims,...
                        pts_working(2,:)*resize_pos(4)/dims, 'g.',...
                        'MarkerSize', 2 );
                elseif(isempty(this.pts_plot) && isempty(pts_set))
                    axes(this.h_axes), hold on
                    this.pts_plot = plot( pts_working(1,:)*resize_pos(3)/dims,...
                        pts_working(2,:)*resize_pos(4)/dims, 'g.',...
                        'MarkerSize', 2 );
                    
                elseif(isempty(this.pts_plot) && isempty(pts_working))
                    axes(this.h_axes), hold on
                    this.pts_plot = plot( pts_set(1,:)*resize_pos(3)/dims,...
                        pts_set(2,:)*resize_pos(4)/dims, 'b.',...
                        'MarkerSize', 2 );
                else
                    if( length(this.pts_plot) == 2 )
                        this.pts_plot(1).XData = pts_set(1,:)*resize_pos(3)/dims;
                        this.pts_plot(1).YData = pts_set(2,:)*resize_pos(4)/dims;
                        this.pts_plot(2).XData = pts_working(1,:)*resize_pos(3)/dims;
                        this.pts_plot(2).YData = pts_working(2,:)*resize_pos(4)/dims;
                    else
                        if(isempty(pts_set))
                            this.pts_plot.XData = pts_working(1,:)*resize_pos(3)/dims;
                            this.pts_plot.YData = pts_working(2,:)*resize_pos(4)/dims;
                        elseif(isempty(pts_working))
                            this.pts_plot.Color = [0,0,1];
                            this.pts_plot.XData = pts_set(1,:)*resize_pos(3)/dims;
                            this.pts_plot.YData = pts_set(2,:)*resize_pos(4)/dims;
                        else
                            axes(this.h_axes), hold on
                            this.pts_plot = plot( pts_set(1,:)*resize_pos(3)/dims,...
                                pts_set(2,:)*resize_pos(4)/dims, 'b.',...
                                pts_working(1,:)*resize_pos(3)/dims,...
                                pts_working(2,:)*resize_pos(4)/dims, 'g.',...
                                'MarkerSize', 2 );
                            warning('something went wrong')
                        end
                    end
                end
            end
            if( this.test_bool )
                this.add_test_stripes();
            end
            
        end
        function add_test_stripes(this)
            this.test_centers = ...
                this.testManager.stack_centers{this.stack_idx};
            
            if( isempty( this.test_centers ) )
                return
            end
            pts_concat = [];
            resize_pos = get(this.h_axes, 'Position');
            dims = size(this.img_stack, 1);
            rotation_mat = [cosd(this.theta), -sind(this.theta);...
                sind(this.theta), cosd(this.theta)];
            
            for i = 1:length( this.test_centers )
                temp_x = (1:dims*2)-dims;
                temp_y = zeros(1, dims*2);
                
                % generate stripe locations for left and right of center
                rotated_pts_1 = rotation_mat*[temp_x; temp_y];
                rotated_pts_1(1,:) = rotated_pts_1(1,:) + this.test_centers(i)...
                    - this.width/2*sind(this.theta);
                rotated_pts_1(2,:) = rotated_pts_1(2,:) + dims/2 ...
                    + this.width/2*cosd(this.theta);
                [~, out_of_bounds] = find( rotated_pts_1 < 1 );
                rotated_pts_1(:, out_of_bounds) = [];
                [~, out_of_bounds] = find( rotated_pts_1 > dims );
                rotated_pts_1(:, out_of_bounds) = [];
                
                rotated_pts_2 = rotation_mat*[temp_x; temp_y];
                rotated_pts_2(1,:) = rotated_pts_2(1,:) + this.test_centers(i)...
                    + this.width/2*sind(this.theta);
                rotated_pts_2(2,:) = rotated_pts_2(2,:) + dims/2 ...
                    - this.width/2*cosd(this.theta);
                [~, out_of_bounds] = find( rotated_pts_2 < 1 );
                rotated_pts_2(:, out_of_bounds) = [];
                [~, out_of_bounds] = find( rotated_pts_2 > dims );
                rotated_pts_2(:, out_of_bounds) = [];
                
                pts_concat = [pts_concat, rotated_pts_1, rotated_pts_2];
                
            end
            
            %             if(isempty(this.test_pts_plot) && ~isempty(pts_concat))
            this.test_pts_plot = plot( pts_concat(1,:)*resize_pos(3)/dims,...
                pts_concat(2,:)*resize_pos(4)/dims,...
                'r.', 'MarkerSize', 2 );
            %             else
            %                 this.test_pts_plot.XData = pts_concat(1,:)*resize_pos(3)/dims;
            %                 this.test_pts_plot.XData = pts_concat(2,:)*resize_pos(4)/dims;
            %             end
        end
        function shift_stack_idx(this, shift)
            %             this.pts_plot = [];
            this.stack_idx = this.stack_idx + shift;
            if( this.stack_idx > size(this.img_stack, 3) )
                this.stack_idx = 1;
            elseif( this.stack_idx < 1 )
                this.stack_idx = size(this.img_stack, 3);
            end
            this.centers = this.stack_centers{this.stack_idx};
            this.centers = this.stack_centers{this.stack_idx};
            if(this.test_bool)
                this.test_centers = this.testManager.stack_centers{this.stack_idx};
            end
            this.stripe_set_bool = ones(length(this.centers), 1);
            this.curr_stripe_idx = 0;
            if(this.suppress_bool)
                while( isempty(this.centers) &&...
                        (~this.test_bool || isempty(this.test_centers)) )
                    this.stack_idx = this.stack_idx + shift;
                    if( this.stack_idx == 5 )
                        fprintf('debug point')
                    end
                    if( this.stack_idx > size(this.img_stack, 3) )
                        this.stack_idx = 1;
                    elseif( this.stack_idx < 1 )
                        this.stack_idx = size(this.img_stack, 3);
                    end
                    
                    this.centers = this.stack_centers{this.stack_idx};
                    if(this.test_bool)
                        this.test_centers = this.testManager.stack_centers{this.stack_idx};
                    end
                    this.stripe_set_bool = ones(length(this.centers), 1);
                    this.curr_stripe_idx = 0;
                    %                     if(isempty(this.centers))
                    %                         this.curr_stripe_idx = 0;
                    %                     else
                    %                         this.curr_stripe_idx = 1;
                    %                         this.stripe_set_bool(this.curr_stripe_idx) = 0;
                    %                     end
                end
            end
            this.idx_text.String = sprintf('image idx: %i/%i',...
                this.stack_idx,...
                size(this.img_stack,3));
            this.stripe_idx_text.String = sprintf('stripe idx: %i', this.curr_stripe_idx);
            update_axes(this);
        end
        function update_theta(this, src, evnt)
            this.theta = src.Value;
            update_stripe_extents(this);
            this.theta_text.String = sprintf('theta = %.1f', this.theta);
        end
        function update_width(this, src, evnt)
            this.width = src.Value;
            this.width_text.String = sprintf('width = %.1f', this.width);
            update_stripe_extents(this);
        end
        function update_center(this, src, evnt)
            %             this.pts_plot = [];
            if( this.curr_stripe_idx > 0 )
                this.centers(this.curr_stripe_idx) = src.Value;
            else
                warning('no stripe being edited')
            end
            this.stack_centers{this.stack_idx} = this.centers;
            update_stripe_extents(this);
        end
        function shift_stripe_idx(this, val)
            %             this.pts_plot = [];
            if( this.curr_stripe_idx == 0 && val > 0 )
                if( isempty(this.centers) )
                    warning('no stripes defined');
                    return;
                end
                this.curr_stripe_idx = 1;
            elseif( this.curr_stripe_idx == 0 && val < 0 )
                this.curr_stripe_idx = length(this.centers);
            else
                this.curr_stripe_idx = this.curr_stripe_idx + val;
            end
            if( this.curr_stripe_idx > length( this.centers ) )
                this.curr_stripe_idx = 1;
            elseif( this.curr_stripe_idx < 1 )
                this.curr_stripe_idx = length(this.centers);
            end
            
            for i = 1:length(this.pts_plot)
                this.pts_plot(i).XData = [];
                this.pts_plot(i).YData = [];
            end
            this.stripe_set_bool = ones(length(this.centers), 1);
            this.stripe_set_bool(this.curr_stripe_idx) = 0;
            this.stripe_idx_text.String = sprintf('stripe idx: %i', this.curr_stripe_idx);
            this.h_center_slider.Value = this.centers(this.curr_stripe_idx);
            update_stripe_extents(this);
        end
        
        %% auxiliary function
        function export_labels(this)
            % for some reason doesn't like that last \n
            %             fprintf('\n');
            this.export_struct.stack = this.img_stack;
            this.export_struct.theta = this.theta;
            this.export_struct.width = this.width;
            this.export_struct.stack_centers = this.stack_centers;
            this.export_struct.image_filename = this.image_filename;
            this.export_struct.directory = this.image_directory;
            this.export_struct.stack_idx = this.stack_idx;
            filename = this.image_filename;
            ext_idx = strfind(filename, '.');
            pathname = this.image_directory;
            filename(ext_idx:end) = '.mat';
            
            if( exist(strcat(pathname, filename), 'file' ) )
                overwrite_file = questdlg(sprintf('Overwrite existing file %s?', filename),...
                    'Yes', ...
                    'No');
                if( strcmp(overwrite_file, 'No') )
                    filename = inputdlg('Enter new filename:');
                    if(~strcmp(filename(end-3:end), '.mat'))
                        error('expected .mat extension')
                    end
                end
            end
            label_struct = this.export_struct;
            fprintf('saving data to file %s... \n', filename);
            save(strcat(pathname, filename), 'label_struct');
            fprintf('%s successfully saved\n', filename);
        end
        
        function parse_struct(this, inp_struct)
            this.export_struct = inp_struct;
            this.theta = inp_struct.theta;
            this.img_stack = inp_struct.stack;
            this.width = inp_struct.width;
            this.stack_centers = inp_struct.stack_centers;
            this.image_filename = inp_struct.image_filename;
            this.image_directory = inp_struct.directory;
            this.stack_idx = inp_struct.stack_idx;
            %   this.stripe_set_bool = inp_struct.stripe_set_bool;
        end
        
    end
    
    
end
